CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Instagram Entity Integration module provides an entity type for Instagram
media as well as functionality for fetching and populating the entity data via
the Facebook Graph API.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ig_entity

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/ig_entity

REQUIREMENTS
------------

This module requires the following modules:

 * Facebook Graph API (https://www.drupal.org/project/facebook_graph_api)

Additionally, you will need the following to query the Instagram posts via the
Facebook Graph API.

 * An Instagram business account
 * A Facebook page linked to the Instagram business account
 * A Facebook user with a role on the Facebook page
 * A Facebook App configured for use with the "Instagram" product (see
   configuration instructions) and the "manage_pages" permission.

See https://developers.facebook.com/docs/instagram-api/getting-started.

RECOMMENDED MODULES
-------------------

 * Views (https://www.drupal.org/project/views):
   When enabled, Instagram entities may be rendered in Views output as the
   entities are integrated with the Views system.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Add the Instagram product to the Facebook App your site is using, and include
   the "instagram_basic" and "instagram_manage_insights" permissions.

 * If you intend to automatically pull in Instagram media, ensure cron is
   running on your site.

 * Configure user permissions in Administration » People » Permissions:

   - Configure Instagram Entity Integration

     This permission is required to manage the Instagram media entities and
     sources provided by this module.

 * Add a Media Source on Administration » Configuration » Web services »
   Instagram Entities. This allows you to select a Facebook Graph API token,
   and an Instagram account associated with the token for which to query the
   API. Set the username of the Instagram business account you'd like to load
   media from and save. Future enhancements may allow for additional tokens to
   be utilized and a pluggable system to support additional types of API
   queries.

MAINTAINERS
-----------

Current maintainers:
 * Will Long (kerasai) - https://www.drupal.org/u/kerasai

This project has been sponsored by:
 * Xeno Media, Inc.
   Visit https://www.xenomedia.com/ for more information.
