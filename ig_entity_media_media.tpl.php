<?php

/**
 * @file
 * Default theme implementation for media property within media entities.
 */
?>
<?php if ($entity->media_type == 'image'): ?>
  <div class="instagram-media instagram-media-image">
    <img src="<?php print $entity->media_url ?>">
  </div>
<?php elseif ($entity->media_type == 'video'): ?>
  <div class="instagram-media instagram-media-video">
    <video controls>
      <source src="<?php print $entity->media_url ?>">
    </video>
  </div>
<?php else: ?>
  <div class="instagram-media">
    <?php print t('Rendering not supported for this media type.') ?>
  </div>
<?php endif; ?>
