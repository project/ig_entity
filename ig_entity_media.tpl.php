<?php

/**
 * @file
 * Default theme implementation for Instagram media entities.
 */
?>
<div class="<?php print $classes?>">

    <?php print $media ?>

    <div class="instagram-media-caption">
      <?php print $caption ?>
    </div>

    <div class="instagram-media-user">
      <?php print $username_link ?>
    </div>

    <div class="instagram-media-permalink">
      <?php print $permalink_link ?>
    </div>

</div>
