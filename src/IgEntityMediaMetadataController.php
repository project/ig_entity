<?php

/**
 * Class IgEntityMediaMetadataController.
 */
class IgEntityMediaMetadataController extends EntityDefaultMetadataController {

  /**
   * {@inheritdoc}
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info['ig_entity_media']['properties'];

    $properties['media_url']['type'] = 'uri';
    $properties['permalink']['type'] = 'uri';

    $properties['media'] = array(
      'label' => t("Rendered media"),
      'type' => 'text',
      'description' => t("The Instagram media's rendered media."),
      'entity views field' => TRUE,
      'computed' => TRUE,
      'getter callback' => 'entity_property_ig_entity_media_get',
      'sanitized' => TRUE,
    );

    $properties['caption']['type'] = 'text';
    $properties['caption']['label'] = t('Instagram media caption');
    $properties['caption']['description'] = t('Instagram media "caption" property.');
    $properties['caption']['schema field'] = 'caption';
    $properties['caption']['getter callback'] = 'entity_property_ig_entity_media_get';
    $properties['caption']['sanitized'] = TRUE;

    $properties['timestamp']['type'] = 'date';

    return $info;
  }

}
