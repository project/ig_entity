<?php

/**
 * Class IgEntityApi.
 */
class IgEntityApi {

  /**
   * Gets the Facebook pages.
   *
   * @return array
   *   Facebook page data.
   */
  public function getPages(FacebookGraphApiToken $token) {
    $data = $this->getFacebook()
      ->get('/me/accounts', $token->token)
      ->getDecodedBody();
    return !empty($data['data']) ? $data['data'] : [];
  }

  /**
   * Gets Instagram users found via token.
   *
   * @param \FacebookGraphApiToken $token
   *   The Facebook Graph API token.
   * @param bool $include_page
   *   Include the page name.
   *
   * @return array
   *   The Instagram users accessible via the token.
   */
  public function getIgUsers(FacebookGraphApiToken $token, $include_page = FALSE) {
    $pages = $this->getPages($token);
    $users = [];
    foreach ($pages as $page) {
      $endpoint = "/{$page['id']}?fields=instagram_business_account";
      $data = $this->getFacebook()
        ->get($endpoint, $token->token)
        ->getDecodedBody();
      if (!empty($data["instagram_business_account"]["id"])) {
        $item = $this->getItem($data["instagram_business_account"]["id"], 'id,name', $token);
        $users[$item['id']] = $page['name'] . ' » ' . $item['name'];
      }
    }
    return $users;
  }

  /**
   * Gets an item from the API.
   *
   * @param int $id
   *   The item ID.
   * @param string $fields
   *   Optional, a comma-separated list of specific fields to obtain.
   * @param \FacebookGraphApiToken $token
   *   Optional, the Facebook Graph API token. Defaults to using the app token.
   *
   * @return array
   *   The item data.
   */
  public function getItem($id, $fields = NULL, FacebookGraphApiToken $token = NULL) {
    $endpoint = "/$id";
    if ($fields) {
      $endpoint .= "?fields=$fields";
    }

    if ($token) {
      return $this->getFacebook()
        ->get($endpoint, $token->token)
        ->getDecodedBody();
    }

    return $this->getFacebook()
      ->get($endpoint)
      ->getDecodedBody();
  }

  /**
   * Gets the Facebook SDK.
   *
   * @return \Facebook\Facebook
   *   The Facebook SDK.
   */
  protected function getFacebook() {
    return facebook_graph_api_get_facebook();
  }

}
