<?php

/**
 * Class IgEntityMediaSource.
 */
class IgEntityMediaSource extends Entity {

  /**
   * Fetches media data.
   *
   * @return array|false
   *   Data about the media, or FALSE if there was an error.
   */
  public function fetch() {
    if (empty($this->source_type)) {
      throw new InvalidArgumentException(sprintf('No type set on media source.'));
    }

    $function = 'ig_entity_media_source_fetch_' . $this->source_type;
    if (!function_exists($function)) {
      return FALSE;
    }

    $output = $function($this);

    $this->last_fetched = REQUEST_TIME;
    $this->save();

    return $output;
  }

}
