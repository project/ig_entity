<?php

/**
 * @file
 * Instagram Entity Integration administrative page callbacks.
 */

/**
 * Page callback for media sources listing page.
 */
function ig_entity_admin_media_sources_page() {
  $build['table'] = [
    '#theme' => 'table',
    '#header' => [t('Label'), t('Last fetched'), t('Actions')],
    '#empty' => t('No media sources created.'),
  ];

  $query = db_select('ig_entity_media_source', 'ms')->extend('PagerDefault');
  $query->fields('ms', ['id'])->orderBy('id')->limit(50);
  $ids = $query->execute()->fetchCol();

  /** @var \IgEntityMediaSource[] $sources */
  $sources = entity_load('ig_entity_media_source', $ids);
  foreach ($sources as $source) {
    $row = [
      check_plain($source->label()),
      $source->last_fetched !== NULL ? format_date($source->last_fetched) : t('Never'),
    ];
    $links = [];
    $links[] = l(t('Fetch'), 'admin/config/services/ig-entity/media-source/' . $source->id . '/fetch');
    $links[] = l(t('Edit'), 'admin/config/services/ig-entity/media-source/' . $source->id . '/edit');
    $links[] = l(t('Delete'), 'admin/config/services/ig-entity/media-source/' . $source->id . '/delete');
    $row[] = implode(' ', $links);
    $build['table']['#rows'][] = $row;
  }

  $build['pager'] = ['#theme' => 'pager'];

  return $build;
}

/**
 * Page callback for media listing page.
 */
function ig_entity_admin_media_page() {
  $build['table'] = [
    '#theme' => 'table',
    '#header' => [
      t('Media'),
      t('Type'),
      t('Instagram User'),
      t('Permalink'),
      t('Timestamp'),
    ],
    '#empty' => t('No media available.'),
  ];

  $query = db_select('ig_entity_media', 'm')->extend('PagerDefault');
  $query->fields('m', ['id'])
    ->orderBy('timestamp', 'DESC')
    ->limit(50);
  $ids = $query->execute()->fetchCol();

  /** @var \Entity[] $media */
  $media = entity_load('ig_entity_media', $ids);
  foreach ($media as $item) {
    // Handle thumbnail.
    $w = entity_metadata_wrapper('ig_entity_media', $item);
    $thumbnail = $w->media->value();

    $row = [
      $thumbnail,
      check_plain($item->media_type),
      l($item->username, 'https://www.instagram.com/' . $item->username),
      l(t('Permalink'), $item->permalink),
      format_date($item->timestamp),
    ];

    $build['table']['#rows'][] = $row;
  }

  $build['pager'] = ['#theme' => 'pager'];

  return $build;
}
